class ChartsController < ApplicationController
  def sporters_by_age
    result = Sporter.group(:age).count
    render json: [{name: 'Count', data: result}]
  end

  def sporters_by_country
    result = {}
    Sporter.group(:country).count.each do |country, count|
      result[country.name] = count
    end
    render json: [{name: 'Count', data: result}]
  end

  def competitions_by_year
    result = CompetitionResult.group_by_year(:created_at, format: "%Y").count
    render json: [{name: 'Count', data: result}]
  end
end
